#!/bin/bash

#MASSPOINTS="400 700 900 1100 1300 1600 1900 2200 2500"
MASSPOINTS="1500"
KAPPAS="0 1"
BETAS="0_4 1_0"

for mass in $MASSPOINTS
do
  for kappa in $KAPPAS
  do
    for beta in $BETAS
    do
      echo ">>>> Submit M"${mass}"_k"${kappa}"_b"${beta}
      cd M${mass}_k${kappa}_b${beta}/
      python WriteCondorScripts.py 
      source condor_submission_M${mass}_k${kappa}_b${beta}.sh  
      cd ..
    done
  done
done
