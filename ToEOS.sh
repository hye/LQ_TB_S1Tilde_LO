#!/bin/bash
##move output of derivations to /eos
process="M1000_l0_3"
mkdir /eos/user/h/hye/Leptoquark/DxAOD/Request/${process}
echo "Begin moving DxAOD to /eos"
for i in {1..10}
do
  cd /eos/user/h/hye/Leptoquark/DxAOD/Request/${process}
  mv /afs/cern.ch/work/h/hye/Leptoquark/Production/${process}-${i} .
  cd /afs/cern.ch/work/h/hye/Leptoquark/Production/
done
echo "moving finished"
ls /eos/user/h/hye/Leptoquark/DxAOD/Request/${process}
