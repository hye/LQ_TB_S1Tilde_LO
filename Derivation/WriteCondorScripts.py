#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import os, sys, commands
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-p','--process',help='Process',type=str)
args = parser.parse_args()

def WriteCondorScripts(process):
  path = "/afs/cern.ch/work/h/hye/Leptoquark/Production"


  ex_script = open('condor_submission_{}.sh'.format(process), 'w')
  ex_lines = []

  cmd = "mkdir -p {}/Derivation/{}/".format(path,process)
  os.system(cmd)

  sh_script = open(path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sh', 'w')
  if sh_script: print(' Created sh script '+path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sh')
  sub_script = open(path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sub', 'w')
  if sub_script: print(' Created sub script '+path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sub')
  ex_lines.append('\ncondor_submit '+path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sub')
  textlines = ['#!/bin/sh',
      	 '\nexport ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase',
               '\nalias setupATLAS=\'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\'',
               '\ncd {}/Derivation/{}'.format(path,process),
               '\nsetupATLAS',
               '\nasetup 21.2.86.0,AthDerivation,here',
               '\nReco_tf.py --inputEVNTFile {}/{}/EVNT_S1Tilde_{}.root --outputDAODFile LQ_S1Tilde_{}_DAOD_TRUTH.root --reductionConf TRUTH1'.format(path,process,process,process)
              ]
  sublines = ['\n',
              '\nexecutable = '+path+'/Derivation/submissionWorkloadTemp/derivation_'+process+'.sh',
              '\narguments = $(ClusterId) $(ProcId)',
              '\noutput = '+path+'/Derivation/output/'+process+'.$(ClusterId).$(ProcId).out',
              '\nerror = '+path+'/Derivation/error/'+process+'.$(ClusterId).$(ProcId).err',
              '\nlog = '+path+'/Derivation/log/'+process+'.$(ClusterId).log',
              '\ngetenv = True',
              '\n+JobFlavour = \"tomorrow\"',
              '\nqueue'
             ]
  sh_script.writelines(textlines)
  sub_script.writelines(sublines)
  sh_script.close()
  sub_script.close()

  ex_script.writelines(ex_lines)
  ex_script.close()
 
  return


if __name__ == '__main__':
  process = args.process
  WriteCondorScripts(process)



