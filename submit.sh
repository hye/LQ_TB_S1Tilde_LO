#!/bin/bash

MASSPOINTS="1600 1900"
#COUPLINGS="1_0 1_5 1_7 2_0 2_5"
COUPLINGS="0_5"

for mass in $MASSPOINTS
do
  for lambda in $COUPLINGS
  do
    echo ">>>> Submit M"${mass}"_l"${lambda}
    cd M${mass}_l${lambda}/
    python WriteCondorScripts.py 
    source condor_submission_M${mass}_l${lambda}.sh  
    cd ..
  done
done
