LQ S1Tilde Production
======================

The latest version of JO for LQtaub is:
```bash
S1Tilde_leptoquark_jo_21.6.57
```

Mass points
===========

400, 700, 900, 1100, 1300, 1600, 1900, 2200, 2500 GeV


Couplings
=========

1\_0(for 1.0), 1\_5(for 1.5), 1\_7(for 1.7), 2\_0(for 2.0), 2\_5(for 2.5)


Prepare the production folder and run the EVNT production
---------------------------------------------------------
You need to specify the mass points, coupling points and JO in the file of runLocal.sh to produce submit files for running on condor.
```bash
source runLocal.sh
```
Then, submit jobs.

```bash
source submit.sh
```
Note, you also need to specify the mass points, coupling points and JO in the file of submit.sh

Run the TRUTH1 derivation
-------------------------
Once the EVNT production finished, the TRUTH1 derivation can be launched.
Go into the folder, Derivation.
```bash
cd Derivation
```
Launch the TRUTH1 derivation for one signal.
For example, launch the TRUTH1 derivation for the signal with mass of 900 GeV and coupling of 1.0
```bash
python WriteCondorScripts.py -p M900_l1_0
```
Then, submit the job
```bash
source condor_submission_M900_l1_0.sh 
```
