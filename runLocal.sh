#!/bin/bash

#MASSPOINTS="400 700 900 1100 1300 1600 1900 2200 2500"
MASSPOINTS="1600 1900"
#COUPLINGS="1_0 1_5 1_7 2_0 2_5"
COUPLINGS="0_5"
JobOption="S1Tilde_leptoquark_jo_21.6.57"
#JobOption="S1Tilde_leptoquark_jo_21.6.57_interference"
#JobOption="S1Tilde_leptoquark_jo_21.6.57_InclusiveB"

echo "JobOption: ${JobOption}"

for mass in $MASSPOINTS
do
  for lambda in $COUPLINGS
  do
    source ./proLocal.sh ${JobOption} ${mass} ${lambda}
  done
done
