#!/bin/bash

MASSPOINTS="1600 1900"
#COUPLINGS="1_0 1_5 1_7 2_0 2_5"
COUPLINGS="0_5"

DATE="20220909"

cd /afs/cern.ch/work/h/hye/Leptoquark/Request/
pwd
if [ -d ${DATE} ]
then
  rm -r ${DATE}/
fi    
mkdir -p ${DATE}/100xxx/ ${DATE}/CommonFiles/
cd ${DATE}/CommonFiles/
pwd
cp /afs/cern.ch/work/h/hye/Leptoquark/Production/S1Tilde_leptoquark_jo_21.6.57/LQ_TB_S1Tilde_LO_NoFilter.py .
cd ../100xxx/
pwd
COUNTER=0
for mass in $MASSPOINTS
do
  for lambda in $COUPLINGS
  do
    if [ ${mass} = "900" ] || [ ${mass} = "1600" ] || [ ${mass} = "2500" ]
    then 
      if [ ${lambda} = "1_0" ] || [ ${lambda} = "2_0" ]
      then
        continue
      fi
    fi
    echo ">>>> Prepare M"${mass}"_l"${lambda}
    pwd
    let COUNTER=$COUNTER+1
    echo $COUNTER
    LENGTH=`echo $COUNTER|awk '{print length($0)}'`
    if [ $LENGTH = 1 ]
    then
      FOLDER="10000"$COUNTER
    fi
    if [ $LENGTH = 2 ]
    then
      FOLDER="1000"$COUNTER
    fi
    echo $FOLDER
    mkdir ${FOLDER}/
    cd ${FOLDER}/
    pwd
    cp /afs/cern.ch/work/h/hye/Leptoquark/Production/M${mass}_l${lambda}/log.generate .
    cp /afs/cern.ch/work/h/hye/Leptoquark/Production/M${mass}_l${lambda}/mc.MGPy8EG_S1TbLQtau_NoFilter_M${mass}_l${lambda}.py .
    #mv mc.MGPy8EG_S1TbLQtau_M${mass}_l${lambda}.py mc.MGPy8EG_S1TbLQtau_NoFilter_M${mass}_l${lambda}.py
    #sed -i "s/.py/_NoFilter.py/g" mc.MGPy8EG_S1TbLQtau_NoFilter_M${mass}_l${lambda}.py
    cd ..
    
  done
done

cd /afs/cern.ch/work/h/hye/Leptoquark/Production/
