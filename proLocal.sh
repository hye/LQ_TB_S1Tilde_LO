#!/bin/bash
#Modify files of different mass points
#And submit batch jobs

jo=$1
mass=$2
lamda=$3


process="M"$mass"_l"$lamda
ProducFolder=$process"/"

if [ -d ${ProducFolder} ]
then
    rm -rf ${ProducFolder}
fi 

echo 'Preparing production folder: '${ProducFolder}
echo '***************************************'
cp -r ./${jo} ${ProducFolder}
cd ${ProducFolder}
mv mc.MGPy8EG_S1TbLQtau_NoFilter_M2000_l0_3.py mc.MGPy8EG_S1TbLQtau_NoFilter_M${mass}_l${lamda}.py 
sed -i "s/M2000_l0_3/${process}/g" run_S1Tilde_LO_tau.sh
sed -i "s/M2500_l0_3/${process}/g" WriteCondorScripts.py

cd ..

