#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import os, sys, commands

def WriteCondorScripts(process):
  path = "/afs/cern.ch/work/h/hye/Leptoquark/Production"


  ex_script = open('condor_submission_{}.sh'.format(process), 'w')
  ex_lines = []

  #cmd = "mkdir -p {}/Derivation/{}/".format(path,process)
  #os.system(cmd)

  sh_script = open(path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sh', 'w')
  if sh_script: print(' Created sh script '+path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sh')
  sub_script = open(path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sub', 'w')
  if sub_script: print(' Created sub script '+path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sub')
  ex_lines.append('\ncondor_submit '+path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sub')
  textlines = ['#!/bin/sh',
      	       '\nexport ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase',
               '\nalias setupATLAS=\'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\'',
               '\ncd {}/{}'.format(path,process),
               '\nsource {}/{}/setup.sh'.format(path,process),
               '\nsource {}/{}/run_S1Tilde_LO_tau.sh'.format(path,process),
              ]
  sublines = ['\n',
              '\nexecutable = '+path+'/'+process+'/submissionWorkloadTemp/derivation_'+process+'.sh',
              '\narguments = $(ClusterId) $(ProcId)',
              '\noutput = '+path+'/'+process+'/batchOutput/output/'+process+'.$(ClusterId).$(ProcId).out',
              '\nerror = '+path+'/'+process+'/batchOutput/error/'+process+'.$(ClusterId).$(ProcId).err',
              '\nlog = '+path+'/'+process+'/batchOutput/log/'+process+'.$(ClusterId).log',
              '\ngetenv = True',
              '\n+JobFlavour = \"tomorrow\"',
              '\nqueue'
             ]
  sh_script.writelines(textlines)
  sub_script.writelines(sublines)
  sh_script.close()
  sub_script.close()

  ex_script.writelines(ex_lines)
  ex_script.close()
 
  return


if __name__ == '__main__':
  process = 'M2500_l0_3'
  WriteCondorScripts(process)



