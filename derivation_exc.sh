#!/bin/bash
#Modify files of different mass points
#And submit batch jobs
process="M1000_l0_3"

for i in {1..10}
do
  dir=$process"-"$i
  cd $dir
  mkdir Derivation
  cd Derivation
  cp /afs/cern.ch/work/h/hye/Derivation/derivation.sh .
  cp ../batch_* .
  cp ../*.sub .
  mkdir log/ error/ output/
  sed -i "s/M1000_l0_3/${process}-${i}/g" derivation.sh
  sed -i "s/Production\/${process}-${i}/Production\/${process}-${i}\/Derivation/g" batch_${process}-${i}_cmd.sh
  sed -i "s/setup.sh/derivation.sh/g" batch_${process}-${i}_cmd.sh
  sed -i '16,$d' batch_${process}-${i}_cmd.sh
  source batch_${process}-${i}_exc.sh
  cd ../../
done

