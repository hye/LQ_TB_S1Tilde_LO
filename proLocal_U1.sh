#!/bin/bash
#Modify files of different mass points
#And submit batch jobs

jo=$1
mass=$2
kappa=$3
beta=$4

process="M"$mass"_k"$kappa"_b"$beta
ProducFolder=$process"/"

if [ -d ${ProducFolder} ]
then
    rm -rf ${ProducFolder}
fi 

echo 'Preparing production folder: '${ProducFolder}
echo '***************************************'
cp -r ./${jo} ${ProducFolder}
cd ${ProducFolder}
mv mc.MGPy8EG_U1bLQ2tau_M1200_K0_23L0_00_33L0_40_33R0_0.py mc.MGPy8EG_U1bLQ2tau_M${mass}_K${kappa}_23L0_00_33L${beta}0_33R0_0.py 
sed -i "s/M1200_k0_b0_4/${process}/g" run_U1_LO_tau.sh
sed -i "s/M1200_k0_b0_4/${process}/g" WriteCondorScripts.py

cd ..

