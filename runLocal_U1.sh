#!/bin/bash

#MASSPOINTS="400 700 900 1100 1300 1600 1900 2200 2500"
MASSPOINTS="1500"
KAPPAS="0 1"
BETAS="0_4 1_0"
JobOption="U1_leptoquark_jo"

echo "JobOption: ${JobOption}"

for mass in $MASSPOINTS
do
  for kappa in $KAPPAS
  do
    for beta in $BETAS
    do
      source ./proLocal_U1.sh ${JobOption} ${mass} ${kappa} ${beta}
    done
  done
done
