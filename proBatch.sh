#!/bin/bash
#Modify files of different mass points
#And submit batch jobs

mass=$1
lamda=$2


process="M"$mass"_l"$lamda
ProducFolder=$process"/"

if [ -d ${ProducFolder} ]
then
    rm -rf ${ProducFolder}
fi 

#echo '***************************************'
#echo 'Preparing production folder: '${ProducFolder}
#echo '***************************************'
#cp -r ./S1Tilde_leptoquark_jo_21.6.57 ${ProducFolder}
#cd ${ProducFolder}
#mv mc.MGPy8EG_S1TbLQtau_M2000_l0_3.py mc.MGPy8EG_S1TbLQtau_M${mass}_l${lamda}.py 
#source setup.sh

#echo '***************************************'
#echo 'Start to produce this signal: '${ProducFolder}
#echo '***************************************'
#source run_S1Tilde_LO_tau.sh 
#cd ..

#echo '***************************************'
#echo 'Finish producing this signal: '${ProducFolder}
#echo '***************************************'

for i in {1..10}
do
  dir=$process"-"$i
  cp -r ./S1Tilde_leptoquark_jo_21.6.57 $dir
  cd $dir
  mkdir output/ log/ error/
  newcmdfile="batch_"$process"-"$i"_cmd.sh"
  newexcfile="batch_"$process"-"$i"_exc.sh"
  newsubfile=$process"-"$i".sub"
  newJOfile="mc.MGPy8EG_S1TbLQtau_"$process".py"
  mv batch_M1000_l0_3_cmd.sh $newcmdfile
  mv batch_M1000_l0_3_exc.sh $newexcfile
  mv M1000_l0_3.sub $newsubfile
  mv mc.MGPy8EG_S1TbLQtau_M2000_l0_3.py $newJOfile
  sed -i "s/Production\/M1000_l0_3/Production\/${process}-"$i"/g" batch_${process}-${i}_cmd.sh
  sed -i "s/M1000_l0_3.root/${process}-"$i".root/g" batch_${process}-${i}_cmd.sh
  sed -i "s/M1000_l0_3.py/${process}.py/g" batch_${process}-${i}_cmd.sh
  sed -i "s/M1000_l0_3/${process}-"$i"/g" batch_${process}-${i}_exc.sh
  sed -i "s/--randomSeed=1/--randomSeed="$i"/g" batch_${process}-${i}_cmd.sh
  sed -i "s/M1000_l0_3/${process}-"$i"/g" ${process}-${i}.sub
  source setup.sh
  source batch_${process}-${i}_exc.sh
  cd ..
done

